package Week03;

import java.util.Scanner;

public class HandlingAssignment {

    public static void main(String[] args) {
//Query User for inputs
        Scanner in = new Scanner(System.in);

        System.out.print("Input first number: ");
        float num1 = in.nextInt();

        System.out.print("Input second number: ");
        float num2 = in.nextInt();
// try-catch to keep user from entering a 0.  Terminate program if a 0 is entered.
        try {
            float solve = num1 / num2;
        } catch (ArithmeticException e) {
            System.out.println("Try a different number, you can't divide by 0");
        }
//Out put the true answer when 0 is not entered as the second number.
        float solve = num1 / num2;
        System.out.println("The answer is " + solve);
    }
}